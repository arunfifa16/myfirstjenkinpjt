package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyLeadsPage;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName="TC001_LoginLogout";
		testDescription="Login into Leaftaps";
		testNodes="Leads";
		author="Arun";
		category="smoke";
        dataSheetName="TC001_LoginLogout";
        CreateLeaddataSheet="CreateLeaddata";
	}
		
	@Test(dataProvider="fetchData")
	public void createLead(String username,String password,String fname,String lname,String cname)
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();
		new HomePage().clickCRM_SFA()
		.clickMyLeads()
		.clickCreateLead()
		.enterFirstName(fname)
		.enterLastName(lname)
		.enterCompanyName(cname)
		.clickCreatelead()
		.verifyFirstElement(fname);
	}
}
