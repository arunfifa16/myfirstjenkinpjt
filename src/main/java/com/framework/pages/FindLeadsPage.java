package com.framework.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	
	public FindLeadsPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.NAME,using="id") WebElement eleLeadId;
		public FindLeadsPage enterLeadId(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(eleLeadId,data);
			return this;
		}
		
		@FindBy(how = How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLeadsbutton;
		public FindLeadsPage clickFindLeads() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleFindLeadsbutton);
			return this;
		}
	
		@FindBy(how = How.XPATH,using="/table[@class='x-grid3-row-table']") WebElement LeadsTable;
		@FindBy(how = How.TAG_NAME,using="tr") List<WebElement> rows;
		public FindLeadsPage clickLeadid()
		{
				
			
			
			
			
			return null;
			
			
			
			
			
			
			
		}
	
	

}
