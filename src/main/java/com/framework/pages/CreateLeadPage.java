package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	
	public CreateLeadPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement elefirstName;
		public CreateLeadPage enterFirstName(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(elefirstName, data);
			return this;
		}
		
		@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement elelastName;
		public CreateLeadPage enterLastName(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(elelastName, data);
			return this;
		}
		
		@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
		public CreateLeadPage enterCompanyName(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			clearAndType(eleCompanyName, data);
			return this;
		}
		
		@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleCreatelead;
		public ViewLeadPage clickCreatelead() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleCreatelead);
			return new ViewLeadPage();
		}
		
}
	