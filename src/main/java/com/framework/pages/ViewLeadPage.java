package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{
	
	public ViewLeadPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.XPATH,using="(//span[@class='tabletext'])[4]") WebElement eleFirstName;
		public ViewLeadPage verifyFirstElement(String data) {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			verifyExactText(eleFirstName, data);
			return this;
		}
		
		@FindBy(how = How.XPATH,using="//a[text()='Find Leads']") WebElement eleFindLeads;
		public FindLeadsPage clickFindLeads() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleFindLeads);
			return new FindLeadsPage();
		}
		
		
		
		
		
		
		
}
